#include "le.h"
#include <stdlib.h>

struct llist * create_l(){
  struct llist *head;
  head = malloc(sizeof(struct llist));

  if(head==0){
    return head;
  }

  head->tam  = 0;
  head->head = 0;

  return head;
}

Elem_l * create_node(float val){
  Elem_l *node;
  node = malloc(sizeof(Elem_l));

  node->val = val;
  node->next = 0;

  return node;
}

int insert_l(struct llist *desc, Elem_l * prev, Elem_l * item){
  if(item == 0){
    return 0;
  }else if(prev == 0){
    item->next = desc->head;
    desc->head = item;
    desc->tam++;
    return 1;
  }else{
    item->next = prev->next;
    prev->next = item;
    desc->tam++;
    return 1;
  }
}

int delete_l(struct llist *desc, Elem_l * prev){
  Elem_l *aux;
  if(desc->tam==0){
    return 0;
  }else if(prev==0){
    if(desc->head != 0){
      aux = desc->head->next;
      free (desc->head);
      desc->head = aux;
      desc->tam --;
      return 1;
    }
    desc->head = 0;
    desc->tam = 0;
    return 1;
  }else{
    aux = prev->next->next;
    free(prev->next);
    prev->next = aux;
    desc->tam--;
    return 1;
  }
}

Elem_l * get_l(struct llist *desc, int pos){
  if(pos>desc->tam){
    return 0;
  }
  Elem_l *aux = desc->head;
  int i = 1;
  while(aux->next != 0 && i != pos && i<pos){
    aux = aux->next;
  }
  return aux;
}

//int set_l(struct llist *desc, int pos, int val){
//  Elem_l *node = get_l(desc,pos);
//
//  if(node == 0){
//    return 0;
//  }
//  node->val = val;
//
//  return 1;
//}

//Elem_l * locate_l(struct llist *desc, Elem_l * prev, int val){
//  Elem_l *node;
//
//  if(desc->head == 0)
//    return 0;
//    if(prev == 0){
//      node = desc->head;
//    }else
//      node = prev;
//  while(node->next != 0){
//    if(node->val == val){
//      return node;
//    }
//    node = node->next;
//  }
//  if(node->val == val){
//    return node;
//  }
//  return node->next;
//}

int length_l(struct llist *desc){
  return desc->tam;
}

void destroy_l(struct llist *desc){
  while(desc->head != 0){
    delete_l(desc,0);
  }
}
