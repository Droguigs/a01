//
// Created by schia on 01/06/2018.
//

#include "calc.h"
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#define MALFORMADA INT_MIN
#define OK 1

int main(){

  int err;
  float res;

  err = -1;
  res = calculadora("1 + 2", &err);
  printf("3 = %.2f \n", res);
  printf("%d = OK \n", err);
  res = calculadora("2 + ( ( 4 ^ ( 1 / 2 ) ) * (2 * 1 + 2 ) )", &err);
  printf("10 = %.2f \n", res);
  res = calculadora("2+3", &err);
  printf("%d = %.2f \n", err, res);

  return 0;
}