#include "calc.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>

#define ERROR INT_MIN

int calculadora(char * expr, int * err){
  *err = 1;
  if(expr == 0){
    *err = ERROR;
    return *err;
  }

  struct pilha *P;
  P = create();
  int aux, i;
  float result, op1, op2;
  result = 0;

  List *E;

  *err = 1;
  E = CharToList(expr, err);
  if(*err == ERROR){
    return *err;
  }

  *err = 1;
  E = InfToPost(E, err);
  if(*err == ERROR){
    return *err;
  }

  for(i = 0; i < length_ls(E->l1); i++){
    //char = 0 ; int = 1
    aux = (int)get_ls(E->l2, i+1);
    if(aux != 0){
      push(P, get_ls(E->l1, i+1));
    }else{
      aux = (int)get_ls(E->l1, i+1);
      if(vazia(P)){
        *err = ERROR;
        return *err;
      }
      op2 = top(P);
      pop(P);
      if(vazia(P)){
        *err = ERROR;
        return *err;
      }
      op1 = top(P);
      pop(P);
      if (aux == 43) {
        if(op1 == INT_MAX){
          destroy(P);
          *err = ERROR;
          return *err;
        }
        result = op1 + op2;
      }else if (aux == 45) {
        if(op1 == INT_MIN){
          destroy(P);
          *err = ERROR;
          return *err;
        }
        result = op1 - op2;
      }else if (aux == 42) {
        result = op1 * op2;
      }else if (aux == 47) {
        result = op1 / op2;
      }else if (aux == 94) {
        result = (float) pow((double)op1,(double)op2);
      }else if (aux == 37){
        result = op1 / op2;
        op1 = result;
        aux = (int)op1;
        op1 = (-1)*(aux - result);
        result = op1 * op2;
      }
      push(P,result);
    }
  }

  aux = (int)top(P);
  destroy(P);
  return aux;
}

List* InfToPost(List *E, int * err){

  int aux = length_ls(E->l1);
  char auxC;
  List *S;
  struct pilha *P;

  P = create();
  S = malloc(sizeof(List));
  S->l1 = create_ls(aux+1);
  S->l2 = create_ls(aux+1);

  for(int i = 0; i < aux; i++){
    if (get_ls(E->l2, i+1) == 0){
      //char
      auxC = (char)get_ls(E->l1, i+1);
      // '('
      if(auxC == 40){
        push(P, auxC);
      }// ')'
      else if (auxC == 41) {
        if(vazia(P) == 1){
          free (E);
          destroy_ls(E->l1);
          destroy_ls(E->l2);
          destroy(P);
          *err = ERROR;
          return S;
        }
        while(top(P)!= 40){
          if(vazia(P)){
            free (E);
            destroy_ls(E->l1);
            destroy_ls(E->l2);
            destroy(P);
            *err = ERROR;
            return S;
          }
          insert_ls(S->l1,length_ls(S->l1)+1,top(P));
          insert_ls(S->l2,length_ls(S->l2)+1,0);
          pop(P);
          if(vazia(P)){
            free (E);
            destroy_ls(E->l1);
            destroy_ls(E->l2);
            destroy(P);
            *err = ERROR;
            return S;
          }
        }
        pop(P);
      }// '^'
      else if(auxC == 94){
        if(vazia(P) == 1 || top(P) == 40){
          push(P, auxC);
        }else{
          while((vazia(P) == 0 || top(P) == 40 || top(P) == 94) && top(P)!=12){
            if(top(P) == 94) {
              insert_ls(S->l1, length_ls(S->l1) + 1, top(P));
              insert_ls(S->l2, length_ls(S->l2) + 1, 0);
              pop(P);
            }else {
              push(P, 12);
            }
          }
          if(vazia(P) == 0 && top(P)==12) {
            pop(P);
          }
          push(P, auxC);
        }
      }// '*' || '/'
      else if(auxC == 42 || auxC == 47 || auxC == 37){
        if(vazia(P) == 1 || top(P) == 40 || top(P) == 94 ){
          push(P, auxC);
        }else{
          while((vazia(P) == 0 || top(P) == 40 || top(P) == 94 || top(P)==37 ||top(P)== 42 || top(P) == 47) && top(P) !=12){
            if(top(P) == 94 || top(P)== 42 || top(P) == 47 || top(P) == 37) {
              insert_ls(S->l1, length_ls(S->l1) + 1, top(P));
              insert_ls(S->l2, length_ls(S->l2) + 1, 0);
              pop(P);
            }else {
              push(P, 12);
            }
          }
          if(vazia(P) == 0 && top(P)==12) {
            pop(P);
          }
          push(P, auxC);
        }
      }// '+' || '-'
      else if(auxC == 43 || auxC == 45){
        if(vazia(P)==0){
          if(top(P)== 43 || top(P) == 45){
            push(P, auxC);
          }else{
            if (top(P) != 40) {
              insert_ls(S->l1,length_ls(S->l1)+1,top(P));
              insert_ls(S->l2,length_ls(S->l2)+1,0);
              pop(P);
              push(P, auxC);
            }else
              push(P, auxC);

          }
        }else{
          push(P, auxC);
        }
      }

    }else{
      if(get_ls(E->l1, i+1) >= 48 || get_ls(E->l1, i+1)<= 57) {
        //int
        insert_ls(S->l1, length_ls(S->l1)+1, get_ls(E->l1, i + 1));
        insert_ls(S->l2, length_ls(S->l2)+1, 1);
      }else{
        free (E);
        destroy_ls(E->l1);
        destroy_ls(E->l2);
        destroy(P);
        *err = ERROR;
        return S;
      }
    }
  }
  while(vazia(P)==0){
    if(top(P)==40 || top(P)==41){
      //'(' ou ')'
      free (E);
      destroy_ls(E->l1);
      destroy_ls(E->l2);
      destroy(P);
      *err = ERROR;
      return S;
    }else if(top(P)<= 57 && top(P) >= 48) {
      insert_ls(S->l1, length_ls(S->l1)+1, top(P));
      insert_ls(S->l2, length_ls(S->l2)+1, 0);
      pop(P);
    }else if(top(P) == 37 || top(P) == 43 || top(P) == 45 || top(P) == 42 || top(P) == 94 || top(P) == 47){
      // +, -, *, ^, /
      insert_ls(S->l1, length_ls(S->l1)+1, top(P));
      insert_ls(S->l2, length_ls(S->l2)+1, 0);
      pop(P);
    }else{
      free (E);
      destroy_ls(E->l1);
      destroy_ls(E->l2);
      destroy(P);
      *err = ERROR;
      return S;
    }
  }

  destroy_ls(E->l1);
  destroy_ls(E->l2);
  destroy(P);

  free (E);

  return S;
}

List* CharToList(char *expr, int * err){
  int i, aux, eLength;

  List *E;

  E = malloc(sizeof(List));

  eLength = strlen(expr)+1;

  E->l1 = create_ls(eLength);
  E->l2 = create_ls(eLength);

  i = aux = 0;

  while(expr[i]!= 0){
    if (expr[i] >= 48 && expr[i] <= 57) {
      if (aux != 0) {
        aux *= 10;
        aux += (expr[i] - '0');
      }else {
        aux = (expr[i] - '0');
      }
    }else if(expr[i] != 32){
      if (i==0){
        insert_ls(E->l1, length_ls(E->l1)+1, (elem) expr[i]);
        insert_ls(E->l2, length_ls(E->l2)+1, 0);
        aux = 0;
      }else{
        *err = ERROR;
        return E;
      }
    }else{
      i++;
      if (expr[i] >= 48 && expr[i] <= 57) {
        if (aux != 0) {
          aux *= 10;
          aux += (expr[i] - '0');
        } else {
          aux = (expr[i] - '0');
        }
      }else {
        if (aux != 0) {
          insert_ls(E->l1, length_ls(E->l1) + 1, (elem) aux);
          insert_ls(E->l2, length_ls(E->l2) + 1, 1);
        }
        insert_ls(E->l1, length_ls(E->l1) + 1, (elem) expr[i]);
        insert_ls(E->l2, length_ls(E->l2) + 1, 0);
        aux = 0;
      }
    }
    i++;
  }

  if (aux!=0){
    insert_ls(E->l1, length_ls(E->l1) + 1, (elem) aux);
    insert_ls(E->l2, length_ls(E->l2) + 1, 1);
  }

  return E;
}
