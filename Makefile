CC=gcc

CFLAGS=-Wall -Wextra -Werror -O0 -g -std=c11 -I..
LDFLAGS=-lm

all: teste_aluno teste_professor

le.o: le.c
pilha.o: pilha.c
ls.o: ls.c
calc.o: calc.c
test.o: test.c
aluno.o: aluno.c

# coloque outras dependencias aqui

aluno: le.o pilha.o ls.o calc.o aluno.o $(LDFLAGS)

test: le.o pilha.o ls.o calc.o test.o $(LDFLAGS)


teste_aluno: aluno
	./aluno

teste_professor: test
	./test

clean:
	rm -f *.o test aluno

