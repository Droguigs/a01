#ifndef _CALC_H_
#define _CALC_H_

#include "pilha.h"
#include "ls.h"
#include "le.h"

/** Função que recebe uma expressão infix e calcula seu resultado
  * @param expr string terminada por \0 contendo a expressão
  * @param err código de erro
  * @return valor da expressão
  */

typedef struct List{
  struct list *l1;
  struct list *l2;
}List;

int calculadora(char * expr, int * err);

/**Entrada: fila E com expressão infixada
  *Saída: fila S com expressão pós-fixada
  *
  *Pilha P
  *
  *Para cada elemento x retirado de E:
  *   Caso x for:
  *       operando: Coloque em S
  *       ( : coloque em P
  *       ) : tire operandores de S até encontrar (, adicionando-os a S
  *       operador: tire operadores de S enquanto topo de P tiver precedência
  *       maior ou igual a x ou (, coloque em S
  *                 coloque x em P
  *
  *Tire todos os operandos de P e coloque em S
  *    // Se pilha ainda conter ( , a expressão é inválida
  */
List* InfToPost(List *E, int * err);

List* CharToList(char *expr, int * err);

#endif /* _CALC_H_ */
