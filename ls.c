#include <stdlib.h>
#include "ls.h"

struct list * create_ls(int max){
  if(max<1)
    return 0;
  struct list *head;
  head = (struct list*)malloc(sizeof(struct list));

  head->tam = 0;
  head->max = max;

  head->arm = malloc(sizeof(elem)*max);

  return head;
}

int insert_ls(struct list *desc, int pos, elem item){
  if(desc->tam >= desc->max){
    return 0;
  }else if(pos-1 > desc->tam || pos < 1){
    return 0;
  }else{
    for(int i = (desc->tam)-1; i>=pos-1; i--){
      desc->arm[i+1] = desc->arm[i];
    }
    desc->arm[pos-1] = item;
    desc->tam++;
    return 1;
  }
}

int delete_ls(struct list *desc, int pos){
  if(pos > desc->tam){
    return 0;
  }else{
    desc->tam-=1;
    for(int i=pos;i<desc->tam;i++){
      desc->arm[i] = desc->arm[i+1];
    }
    return 1;
  }
}

elem get_ls(struct list *desc, int pos){
  if(pos > desc->tam){
    return 0;
  }else
    return desc->arm[pos-1];
}

int set_ls(struct list *desc, int pos, elem item){
  if(pos > desc->tam || pos < 1){
    return 0;
  }else{
    desc->arm[pos-1] = item;
    return 1;
  }
}

int locate_ls(struct list *desc, int pos, elem item){
  for(int i=pos-1; i < desc->tam; i++){
    if(desc->arm[i] == item){
      return i+1;
    }
  }
  return 0;
}

int length_ls(struct list *desc){
  return desc->tam;
}

int max_ls(struct list *desc){
  return desc->max;
}

int full_ls(struct list *desc){
  if(length_ls(desc)==max_ls(desc)){
    return 1;
  }
  return 0;
}

void destroy_ls(struct list *desc){
  free(desc->arm);
  free(desc);
}
